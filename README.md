# ␀form

␀form – or NULform if your font is lacking – is an experimental IRC bot written
in [Elixir](http://elixir-lang.org/), a language built on top of Erlang. The bot
is only a secondary result of my effort to try to learn a new language and new
programming paradigms.

At this stage (and probably forever), ␀form is not even alpha quality and even
releases in the supposedly stable branches can contain errors and may not even
compile.


## Goals for ␀form

* Functional-ish design
* Distributed, with tasks not blocking others from completion
* Performant and able to scale to many networks and users
* Live code reloading without restarting processes
