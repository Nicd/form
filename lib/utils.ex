defmodule Utils do
    @moduledoc """
        Various utility functions for miscellaneous stuff.
    """

    @doc """
        hn (handle nulform) handles nulform's internal messages that have the
        same implementation for every process. If you need a custom
        implementation for some of them, catch them in the receive block before
        the default rule.
    """
    def hn(tuple) do
        case tuple do
            {:nulform, :die} ->
                # Kill yourself somehow
                :nop

            {:nulform, :report, pid} ->
                # Send report to pid
                :nop


            _ -> # Ignore the rest
                # TODO: Would it be nicer to die here instead?
        end
    end
end