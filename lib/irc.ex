defmodule Nulform.IRC do
    @moduledoc """
    This module contains a simplistic IRC library with records and related
    utilities for processing of IRC messages.
    """

    @doc """
    Message contains one IRC message with information about the connection where
    it is from, the buffer replies should be sent to and the raw string.
    """
    defrecord Message,
        connection_id: 0,
        buffer: nil,
        raw_msg: ""



    # --- Custom message types ---

    @doc """

    """
    defrecord PRIVMSG,
        info: nil,
        sender: "",
        target: "",
        text: "" do

        @doc """
        Create a new PRIVMSG from a Message. Returns nil if Message does not
        contain a PRIVMSG.
        """
        def parse(msg) do
            case String.split msg.raw_msg, " " do
                [sender, "PRIVMSG", target | text] ->
                    <<":", sender :: binary>> = sender
                    new_msg = PRIVMSG.new [info: msg, sender: sender,
                                           target: target,
                                           text: Nulform.Utilities.to_utf8 Enum.join(text, " ")]

                _ ->
                    nil
            end
        end

        @doc """
        Reply to a PRIVMSG. Will send the reply to a channel if the target is
        one, otherwise will reply to sender.

        Returns the raw command as a binary.
        """
        def reply(msg, text) do
            target = nil
            case msg.target do
                <<"#", _ :: binary>> -> target = msg.target
                _ -> [target | _] = String.split msg.sender, "!"
            end
            "PRIVMSG " <> target <> " :" <> text
        end
    end

end
